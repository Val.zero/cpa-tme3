use structopt::StructOpt;
use crate::graph::*;

mod graph;

/// Struct pour la lecture d'arguments
#[derive(StructOpt, Debug)]
#[structopt(name = "config")]
struct Opt {
    /// Number of nodes in the graph
    #[structopt(short = "n", long, default_value = "400")]
    nodes: usize,

    /// Number of clusters in the graph
    #[structopt(short = "c", long, default_value = "4")]
    clusters: usize,

    /// Probability for a node to be neighbor with a node from the same cluster
    #[structopt(short = "p", long, default_value = "0.2")]
    p: f64,

    /// Probability for a node to be neighbor with a node from another cluster
    #[structopt(short = "q", long, default_value = "0.002")]
    q: f64,
}

fn main() {
    let opt = Opt::from_args();
    let g = make_graph(opt.nodes, opt.clusters, opt.p, opt.q);
    let g_prim = label_prop(&g);
    // to_dot(g, "./res.dot");
    let colors = color_values(&g_prim);
    to_dot_colored(g_prim, colors, "./res.dot");
}
