use rand::rngs::OsRng;
use rand::Rng;
use std::collections::HashMap;
use std::fs::File;
use std::io::{BufWriter, Write};

pub fn make_graph(n: usize, c: usize, p: f64, q: f64) -> HashMap<usize, Vec<usize>> {
    assert!(c <= n);
    assert!(p >= q);
    if p < q {
        panic!("p < q")
    }
    let mut res = HashMap::new();
    let mut cluster_start = 0;
    let mut cluster_end = n / c;
    while cluster_end <= n {
        for i in cluster_start..cluster_end {
            let mut neigh = Vec::new();
            for j in (i + 1)..cluster_end {
                if rand::random::<f64>() < p {
                    neigh.push(j);
                }
            }
            for k in cluster_end..n {
                if rand::random::<f64>() < q {
                    neigh.push(k);
                }
            }
            res.insert(i, neigh);
        }
        cluster_start = cluster_end;
        cluster_end += n / c;
    }
    let nodes = res.keys().cloned().collect::<Vec<usize>>();
    for node in nodes {
        let neighbors = res
            .get(&node)
            .unwrap()
            .iter()
            .cloned()
            .collect::<Vec<usize>>();
        for neigh in neighbors {
            let v = res.get_mut(&neigh).unwrap();
            v.push(node);
        }
    }
    res
}

fn find_label(neigh: Vec<usize>, labels: &HashMap<usize, usize>) -> Option<usize> {
    if neigh.is_empty() {
        None
    } else {
        let mut tmp = HashMap::new();
        for n in neigh {
            let label = labels.get(&n).unwrap();
            if tmp.contains_key(label) {
                let val = tmp.get(label).unwrap();
                tmp.insert(label, val + 1);
            } else {
                tmp.insert(label, 0);
            }
        }
        let mut max_label = **tmp.keys().next().unwrap();
        let mut max_val = *tmp.get(&max_label).unwrap();
        for (label, val) in &tmp {
            if *val > max_val {
                max_val = *val;
                max_label = **label;
            }
        }
        Some(max_label)
    }
}

pub fn label_prop(graph: &HashMap<usize, Vec<usize>>) -> HashMap<(usize, usize), Vec<usize>> {
    let mut labels = HashMap::new();
    for node in graph.keys() {
        labels.insert(*node, *node);
    }
    let mut end = false;
    while !end {
        end = true;
        for (node, neigh) in graph {
            let cur_label = labels.get(node).unwrap();
            let neigh_clone = neigh.to_vec();
            let tmp = find_label(neigh_clone, &labels);
            let new_label = tmp.unwrap_or(*cur_label);
            if *cur_label != new_label {
                labels.insert(*node, new_label);
                end = false;
            }
        }
    }
    let mut res = HashMap::new();
    for (node, neigh) in graph {
        res.insert((*node, *labels.get(node).unwrap()), neigh.to_vec());
    }
    res
}

pub fn color_values(graph: &HashMap<(usize, usize), Vec<usize>>) -> HashMap<usize, usize> {
    let mut res = HashMap::new();
    for (_, val) in graph.keys() {
        if !res.contains_key(val) {
            let color = OsRng.gen_range(0..16777216);
            res.insert(*val, color);
        }
    }
    res
}

pub fn to_dot(graph: HashMap<usize, Vec<usize>>, fpath: &str) {
    let f = File::create(fpath).unwrap();
    let mut writer = BufWriter::new(f);
    writer.write("graph {\n".as_bytes()).unwrap();
    for i in 0..graph.len() {
        writer.write(format!("{}\n", i).as_bytes()).unwrap();
    }
    for (node, neighbors) in graph {
        for neigh in neighbors {
            writer
                .write(format!("{} -- {}\n", node, neigh).as_bytes())
                .unwrap();
        }
    }
    writer.write("}".as_bytes()).unwrap();
}

pub fn to_dot_colored(
    graph: HashMap<(usize, usize), Vec<usize>>,
    colors: HashMap<usize, usize>,
    fpath: &str,
) {
    let f = File::create(fpath).unwrap();
    let mut writer = BufWriter::new(f);
    writer.write("graph {\n".as_bytes()).unwrap();
    for (node, label) in graph.keys() {
        writer
            .write(
                format!(
                    "{} [ fillcolor=\"#{:x}\" ]\n",
                    node,
                    colors.get(label).unwrap()
                )
                .as_bytes(),
            )
            .unwrap();
    }
    for ((node, _), neighbors) in graph {
        for neigh in &neighbors {
            if node < *neigh {
                writer
                    .write(format!("{} -- {}\n", node, neigh).as_bytes())
                    .unwrap();
            }
        }
    }
    writer.write("}".as_bytes()).unwrap();
}
