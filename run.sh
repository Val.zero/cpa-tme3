if test $# -eq 0
  then ./target/release/tme3
  elif test $# -eq 1
    then ./target/release/tme3 -n $1
  elif test $# -eq 2
    then ./target/release/tme3 -n $1 -c $2
  elif test $# -eq 4
    then ./target/release/tme3 -n $1 -c $2 -p $3 -q $4
fi

[ -f res.dot ] && python3 visu.py
[ -f res.svg ] && eog res.svg