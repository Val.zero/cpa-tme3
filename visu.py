import pygraphviz as pgv

# for i in range(10):
#     G = pgv.AGraph("res" + str(i) + ".dot")
#     G.node_attr["style"] = "filled"
#     G.node_attr["shape"] = "circle"
#     G.node_attr["fixedsize"] = "true"
#     G.node_attr["fontcolor"] = "#FFFFFF"
#     G.graph_attr["outputorder"] = "edgesfirst"
#
#     G.layout(prog="fdp")
#     G.draw("file" + str(i) + ".svg")

G = pgv.AGraph("res.dot")
G.node_attr["style"] = "filled"
G.node_attr["shape"] = "circle"
G.node_attr["fixedsize"] = "true"
G.node_attr["fontcolor"] = "#FFFFFF"
G.graph_attr["outputorder"] = "edgesfirst"

G.layout(prog="fdp")
G.draw("res.svg")
